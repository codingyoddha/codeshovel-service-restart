from flask import Flask
import subprocess


app = Flask(__name__)


@app.route('/')
def hello():
    subprocess.call("sh ./cron.sh", shell=True)
    return "xy"

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True, port=5000)